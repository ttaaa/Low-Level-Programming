#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rotation.h"
#include "bmp_io.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        puts("Arguments: [input file]");
        return 1;
    }

    const char *input_path = argv[1];
    const char *output_left_path = "out_left.bmp";
    const char *output_right_path = "out_right.bmp";
    const char *output_180_path = "out_180.bmp";

    struct image *in_image = (struct image *) malloc(sizeof(struct image));
    switch (read_picture(input_path, in_image)) {
        case READ_OK: {
            puts("Image is loaded.");
            break;
        }
        case READ_FILENAME_NOT_FOUND: {
            puts("Input file name is not specified");
            return 1;
        }
        case READ_FILE_ERROR: {
            puts("Unable to open input file");
            return 1;
        }
        case READ_INVALID_BITS: {
            puts("Invalid input data");
            return 1;
        }
        case READ_INVALID_HEADER: {
            puts("Input file is not a bmp");
            return 1;
        }
        default: {
            puts("Undefined reading error");
            return 1;
        }
    }

    struct image *out_left_image = rotate_left(in_image);
    switch (write_picture(output_left_path, out_left_image)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    struct image *out_right_image = rotate_right(in_image);
    switch (write_picture(output_right_path, out_right_image)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    struct image *out_double_image = rotate_double(in_image);
    switch (write_picture(output_180_path, out_double_image)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }


    return 0;
}
