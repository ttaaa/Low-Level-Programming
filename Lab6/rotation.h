#ifndef ROTATION_H
#define ROTATION_H

#include "bmp_struct.h"
#include <math.h>

struct image* rotate_left(struct image const* picture);
struct image* rotate_right(struct image const* picture);
struct image* rotate_double(struct image const* picture);

#endif //ROTATION_H
