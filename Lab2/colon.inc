%define lw 0		

%macro colon 2			; начало макроса - принимает 2 параметра: строку и указатель

%%lw: dq lw 			; временная переменная для хранения указателя на след строку
db %1, 0 			
%2:						; метка из 2 параметра

%define lw %%lw			; константа для адреса следующей строки
%endmacro

colon "hello", hello
db "Hello, world!!!", 0 

colon "world", world
db "Earth is our world!", 0

colon "java", java
db "Java is the best language for ever!", 0
