#include "mem.h"

struct teststruct{
	int value;
	struct teststruct* anyLink;
};


int main(){
	heap_init(HEAP_PAGE_SIZE);

	printf("\n==========\nHeap info before all:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");

	struct teststruct *structTesting;
	int *singleint;
	int *intarray;
	int *intToBeMerged;
	
	int i;
	
	singleint = _malloc(sizeof(int));
	intarray = _malloc(sizeof(int)*10);
	intToBeMerged = _malloc(sizeof(int));
	
	printf("sizeof : %lu\n", sizeof(struct mem));
	
	printf("\n==========\nHeap info after alloc:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");


	for(i = 0; i< 10; i++){
		intarray[i] = rand();
	}

	*singleint = 20;
	*intToBeMerged = 500;
	structTesting =  _malloc(sizeof(struct teststruct) * 4);
	memalloc_debug_heap(stdout);
	printf("%p\n", structTesting );
	structTesting[0] = (struct teststruct) {10, structTesting + 1};
	structTesting[1] = (struct teststruct) {64000, structTesting + 2};
	structTesting[2] = (struct teststruct) {-234324,  structTesting + 3};
	structTesting[3] = (struct teststruct) {23423,  structTesting};
	
	printf("\n==========\nHeap info before realloc:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");
	
	structTesting = _realloc(structTesting, sizeof(struct teststruct) * 5);
	printf("%p", structTesting);
	structTesting[4] = (struct teststruct) {49,  structTesting};
	
	printf("\n==========\nHeap info after realloc and fill:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");
	
	
	_free(intToBeMerged);
	
	printf("\n==========\nHeap info after free 1:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");
	
	_free(intarray);
	
	printf("\n==========\nHeap info after free 2:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n");

	_free(structTesting);
	
	printf("\n==========\nHeap info after free 3:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");
	
	_free(singleint);

	printf("\n==========\nHeap info after all free:\n----------\n");
	memalloc_debug_heap(stdout);
	printf("==========\n\n");

	return 0;
}
