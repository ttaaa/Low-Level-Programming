#ifndef _LAB7_MEM_H_
#define _LAB7_MEM_H_

#define _USE_MISC

#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

#define HEAP_START ((void*) 0x04040000)
#define HEAP_PAGE_SIZE (4*1024)
#define BLOCK_MIN_SIZE 32


struct mem;
#pragma pack(push, 1)
struct mem {
	struct mem* next;
	size_t capacity;
	bool is_free;
};
#pragma pack(pop)

void* _malloc(size_t query);
void _free(void* mem);
void* heap_init(size_t initial_size);
void* _realloc(void *ptr, size_t query);


#define DEBUG_FIRST_BYTES 4

void memalloc_debug_struct_info(FILE* f, struct mem const* const address);
void memalloc_debug_heap(FILE* f);

#endif //_LAB7_MEM_H_
