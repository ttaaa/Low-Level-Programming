#include <stdio.h>
#include <stdlib.h>

const int first_array[] = {1, 2, 3, 4, 5, 6};
const int second_array[] = {1, 1, 1, 1, 1, 1};

int main() {
    size_t first_array_length = sizeof(first_array) / sizeof(first_array[0]);
    size_t second_array_length = sizeof(second_array) / sizeof(second_array[0]);
    
    if (first_array_length != second_array_length) {
        printf("Cannot calculate the scalar product of vectors of different dimensions.");
    } else {
        printf("first_array = ");
        print_array(first_array, first_array_length);
        
        printf("second_array = ");
        print_array(second_array, second_array_length);
        
        printf("Inner product: %d\n\n", scalar_product(first_array, second_array, first_array_length));
    }

    return 0;
}

int scalar_product(const int* first_array, const int* second_array, size_t length) {
    int result = 0;
    
    for (size_t i = 0; i < length; i++) {
        result += first_array[i] * second_array[i];
    }

    return result;
}

void print_array(const int* array, size_t array_length){
    printf ("[");
    
    for (size_t i = 0; i < array_length; i++) {
        printf(" %d", array[i]);
        
        if (i != array_length - 1) {
            printf(",");
        }
    }
    
    printf(" ]\n");
}
