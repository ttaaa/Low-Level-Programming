    #include <stdio.h>
#include <stdlib.h>

int main() {
    unsigned long n = 0;
    
    printf("Input unsigned long : ");
    scanf("%lu", &n);
    
    printf("(%lu) Is prime : ", n);
    printf(is_prime(n) ? "Yes\n" : "No\n");
    
    return 0;
}

int is_prime(unsigned long x) {
    if (x == 2) {
        return 1;
    }
        
    if (x % 2 == 0 || x == 1) {
        return 0;
    }

    for (size_t i = 3; i * i <= x; i += 2) {
        if (x % i == 0) {
            return 0;
        }
    }

    return 1;
}