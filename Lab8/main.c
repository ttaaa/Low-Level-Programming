#include <stdio.h>
#include <sys/resource.h>
#include <stdint.h>
#include "bmp_struct.h"
#include "bmp_io.h"
#include "sepia.h"

void invoke_function(void (*function)(struct image*, struct image*), struct image*, struct image*, char* description);
int check_default_img();
int check_big_img();

int main() {
    if (check_default_img() == 1) {
        return 1;
    }
    
    if (check_big_img() == 1) {
        return 1;
    }

    return 0;
}

int check_default_img() {
    const char *input_path = "in.bmp";
    const char *output_path = "out.bmp";
    const char *output_c_path = "out_c.bmp";
    const char *output_sse_path = "out_sse.bmp";
    
    struct image result1;
    struct image result2;

    puts("Check for default image");
    
    struct image *default_img = (struct image *) malloc(sizeof(struct image));
    switch (read_picture(input_path, default_img)) {
        case READ_OK: {
            puts("Image is loaded.");
            break;
        }
        case READ_FILENAME_NOT_FOUND: {
            puts("Input file name is not specified");
            return 1;
        }
        case READ_FILE_ERROR: {
            puts("Unable to open input file");
            return 1;
        }
        case READ_INVALID_BITS: {
            puts("Invalid input data");
            return 1;
        }
        case READ_INVALID_HEADER: {
            puts("Input file is not a bmp");
            return 1;
        }
        default: {
            puts("Undefined reading error");
            return 1;
        }
    }


    switch (write_picture(output_path, default_img)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }


    invoke_function(sepia_c, default_img, &result1, "C");

    switch (write_picture(output_c_path, &result1)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    invoke_function(sepia_sse, default_img, &result2, "SSE");

    switch (write_picture(output_sse_path, &result2)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    puts("End of checking default image\n");

    return 0;
}

int check_big_img() {
    const char *output_in_path = "in_big.bmp";
    const char *output_c_path = "out_big_c.bmp";
    const char *output_sse_path = "out_big_sse.bmp";
    
    struct image result1;
    struct image result2;
    struct image big_img;

    big_img.width = 5000;
    big_img.height = 5000;
    big_img.data = malloc(big_img.width * big_img.height * sizeof(struct pixel));
    
    puts("Constructing big image");
    for (uint64_t x = 0; x < big_img.width; ++x) {
        for (uint64_t y = 0; y < big_img.height; ++y) {
            struct pixel rnd_pixel = {rand() % 256, rand() % 256, rand() % 256};
            set_pixel(&big_img, x, y, rnd_pixel);
        }
    }
    puts("Big image construction finished\n");
    
    switch (write_picture(output_in_path, &big_img)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    invoke_function(sepia_c, &big_img, &result1, "C");

    switch (write_picture(output_c_path, &result1)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    invoke_function(sepia_sse, &big_img, &result2, "SSE");

    switch (write_picture(output_sse_path, &result2)) {
        case WRITE_OK: {
            puts("Image is saved");
            break;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            return 1;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            return 1;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            return 1;
        }
        default: {
            puts("Undefined writing error");
            return 1;
        }
    }

    return 0;
}

void invoke_function(void (*function)(struct image*, struct image*), struct image* img_in, struct image* img_out, char* description) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    function(img_in, img_out);

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res_time = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for %s filter: %.2f\n", description, res_time/1000000.0);

}
