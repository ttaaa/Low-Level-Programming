#include <stdlib.h>
#include "bmp_struct.h"

#ifndef LAB8_SEPIA_H
#define LAB8_SEPIA_H

void sepia_sse(struct image*, struct image*);
void sepia_c(struct image* img, struct image* res);

#endif //LAB8_SEPIA_H
