#include "./linked_list.h"
#include "./higher_order_func.h"
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>

void print_space(int64_t);
void print_new_line(int64_t);
int64_t square(int64_t);
int64_t cube(int64_t);
int64_t multiply(int64_t x, int64_t a);
int64_t ariphmeticalFunction(int64_t x);
void basic_test(struct LinkedList* list);
struct LinkedList* init(void);
void foreach_test(struct LinkedList* list);
void map_test(struct LinkedList* list);
void map_mut_test(struct LinkedList* list);
void foldl_test(struct LinkedList* list);
struct LinkedList* iterate_test(void);
void file_test(struct LinkedList** list);
void file_bin_test(struct LinkedList** list);

// enum status {
//     Ok,
//     ReadingNumbersError
//     SerializationError,
//     DeserializationError,
//     SavingError,
//     LoadingError
// };

struct LinkedList *init(void) {
    int64_t digit;
    char str[20];
    struct LinkedList *list;

    printf("Write your numbers: ");
    
    if (scanf("%20s", str) != EOF) {
    	
    	digit = strtoll(str, NULL, 10);

    	if (digit == 0) {
		    /* If a conversion error occurred, display a message and exit */
		    if (errno == EINVAL) {
		        printf("Conversion error occurred: %d\n", errno);
		        exit(0);
		    }

		    /* If the value provided was out of range, display a warning message */
		    if (errno == ERANGE) {
				printf("The value provided was out of range\n");
		    	exit(0);
		    }
		}


        list = list_create(digit);
        while (scanf("%20s", str) != EOF) {
        	digit = strtoll(str, NULL, 10);

	    	if (digit == 0) {
			    /* If a conversion error occurred, display a message and exit */
			    if (errno == EINVAL) {
			        printf("Conversion error occurred: %d\n", errno);
			        exit(0);
			    }

			    /* If the value provided was out of range, display a warning message */
			    if (errno == ERANGE) {
					printf("The value provided was out of range\n");
			    	exit(0);
			    }
			}

            list_add_front(&list, digit);
        }
    }
    
    return list;
}

void foreach_test(struct LinkedList *list) {
    puts("Foreach with spaces");
    foreach(list, print_space);
    
    puts("\nForeach with new line");
    foreach(list, print_new_line);
}

void basic_test(struct LinkedList* list) {
    int64_t second = list_get_at(list, 2);
    size_t len = list_length(list);
    struct LinkedList* third = list_node_at(list, 3);
    
    int64_t sum = list_sum(list);
    printf("length -> %lu\nsum -> %"PRId64"\n", len, sum);
    
    if ((second != 0) && (third != NULL)) {
        printf("list_get second -> %"PRId64"\nlist_node_at third %"PRId64"\n",
               second,
               third->value);
    } else {
        puts("Exception in list_node_at or list_get");
    }
}

void file_test(struct LinkedList** iter) {
    puts("Saving in file");
    if (!save(*iter, "test")) {
        puts("Saving error");
        exit(0);
    };

    puts("Saved");

    list_free(*iter);
    *iter = NULL;

    puts("Loading from file");
    if (!load(iter, "test")) {
        puts("Loading error");
        exit(0);
    }
    printf("Load is complited, list is -> ");

    foreach(*iter, print_space);
    puts("");
}

void file_bin_test(struct LinkedList** iter) {
    puts("Serialization");
    if (!serialize(*iter, "./testbin.bin")) {
        puts("Bin file serialization error");
        exit(0);
    };
    puts("Serialization complite");

    list_free(*iter);
    *iter = NULL;

    puts("Deserialization");
    if (!deserialize(iter, "./testbin.bin")) {
        puts("Bin file deserialization error");
        exit(0);
    }
    printf("Deserialization complite, list is -> ");
    
    foreach(*iter, print_space);
    puts("");
}

struct LinkedList* iterate_test(void) {
    struct LinkedList *iter = iterate(1, 10, ariphmeticalFunction);
    puts("iteration");
    foreach(iter, print_space);
    puts("");
    return iter;
}

void foldl_test(struct LinkedList* list) {
    int64_t su = foldl(1, list, multiply);
    printf("foldl sum-> %"PRId64"\n", su);
}

void map_mut_test(struct LinkedList* list) {
    map_mut(&list, cube);
    puts("map_mut abs");

    foreach(list, print_space);
    puts("");
}

void map_test(struct LinkedList* list) {
    struct LinkedList* squares = map(list, square);
    struct LinkedList* cubes = map(list, cube);

    puts("map square");
    foreach(squares, print_space);
    puts("");

    puts("map cubes");
    foreach(cubes, print_space);
    puts("");

    list_free(squares);
    list_free(cubes);
}

void test_length() {
	struct LinkedList* s = list_create(0);

	unsigned long long length = 1;

	while (true) {
		list_add_front(&s, length);

		if (list_length(s) != length) {
			break;
		}

		length += 1;
		printf("%"PRId64"\n", list_length(s));
		
	}
}

void print_space(int64_t i) {
    printf("%" PRId64 " ", i);
}

void print_new_line(int64_t i) {
    printf("%" PRId64 "\n", i);
}

int64_t square(int64_t x) {
    return x * x;
}

int64_t cube(int64_t x) {
    return x * x * x;
}

int64_t multiply(int64_t x, int64_t a) {
    return x * a;
}

int64_t ariphmeticalFunction(int64_t x) {
    return 3 * x + 5;
}

int main() {
	struct LinkedList *list = init();
    basic_test(list);
    foreach_test(list);
    map_test(list);
    map_mut_test(list);
    foldl_test(list);
    struct LinkedList *iter = iterate_test();
    file_test(&iter);
    file_bin_test(&iter);
    list_free(list);
    list_free(iter);
    return 0;
}
